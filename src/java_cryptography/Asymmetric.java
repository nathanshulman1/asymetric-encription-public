package java_cryptography;

//import javax.xml.bind.DatatypeConverter;

import javax.crypto.Cipher;
import java.security.*;
import java.util.Scanner;

// Class to create an asymmetric key
public class Asymmetric {

    private static java.lang.String RSA = "RSA";

    // Generating public and private keys
    // using RSA algorithm.
    public static KeyPair generateRSAKkeyPair()
            throws Exception {
        SecureRandom secureRandom
                = new SecureRandom();

        KeyPairGenerator keyPairGenerator
                = KeyPairGenerator.getInstance(RSA);

        keyPairGenerator.initialize(
                2048, secureRandom);

        return keyPairGenerator
                .generateKeyPair();
    }
    // Encryption function which converts
    // the plainText into a cipherText
    // using private Key.
    public static byte[] do_RSAEncryption(
            String plainText,
            PrivateKey privateKey)
            throws Exception {
        Cipher cipher
                = Cipher.getInstance(RSA);

        cipher.init(
                Cipher.ENCRYPT_MODE, privateKey);

        return cipher.doFinal(
                plainText.getBytes());
    }
    // Decryption function which converts
    // the ciphertext back to the
    // original plaintext.
    public static String do_RSADecryption(
            byte[] cipherText,
            PublicKey publicKey)
            throws Exception
    {
        Cipher cipher
                = Cipher.getInstance(RSA);

        cipher.init(Cipher.DECRYPT_MODE, publicKey);
        byte[] result
                = cipher.doFinal(cipherText);

        return new String(result);
    }
    public static String convertByteToHexadecimal(byte[] byteArray)
    {
        String hex = "";

        // Iterating through each byte in the array
        for (byte i : byteArray) {
            hex += String.format("%02X", i);
        }

        return (hex);
    }

    // Driver code
    public static void main(String[] args)
            throws Exception {

        String plainText = null;
        Scanner myScanner = new Scanner(System.in);

        // create the public and private keys
        KeyPair keypair
                = generateRSAKkeyPair();
        System.out.println("text to eyncrypt");
        plainText = String.valueOf(myScanner.nextLine());

        byte[] cipherText = do_RSAEncryption(plainText, keypair.getPrivate());

        //decrypts the text back into a string
        String decryptedText
                = do_RSADecryption(
                cipherText,
                keypair.getPublic());
        System.out.println("The Encrypted Text is: "+ convertByteToHexadecimal(cipherText));

        System.out.println("The decrypted text is: " + decryptedText);

    }
}
